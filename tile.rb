class Tile

  attr_accessor :piece

  def initialize(x, y, color)
    @x = x
    @y = y
    @color = color
  end

  def empty?
    self.piece.nil?
  end

  def not_empty?
    !empty?
  end

  def contains_piece_of_enemy_color? color
    return false if self.empty?
    return false if self.piece.color == color
    return true
  end

  def add_piece(piece)
    self.piece = piece
    piece.tile = self
  end

  def remove_piece
    if not_empty?
      piece.tile = nil
      self.piece = nil
    end
  end

  def coordinates
    [@x,@y]
  end

  def to_s
    "|#{piece.nil? ?  "  " : piece.to_s}|"
  end

  def piece_can_move_here?
    return false if not_empty? or self.is_a?(NilTile)
    return true
  end

  def contains_enemy_king? color
    return true if not_empty? and piece.type == :king and piece.color != color
    return false
  end
end

class NilTile < Tile

  def initialize
    super(nil, nil, nil)
  end

  def add_piece
    raise "Cannot add a piece on a NilTile"
  end

end