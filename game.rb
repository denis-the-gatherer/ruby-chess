class Game

  def initialize
    @board = Board.new
    @move_engine = MoveEngine.new @board
    @game_state = GameState.new @board, @move_engine
    @opponent = AIPlayer.new :black, @board.all_pieces_of_color(:black), @move_engine
  end

  def make_move_by_coordinates from_coordinates, to_coordinates
    from_tile = @board.tile_for_coordinates from_coordinates[0], from_coordinates[1]
    to_tile   = @board.tile_for_coordinates to_coordinates[0], to_coordinates[1]

    make_move from_tile, to_tile
  end

  def make_move from_tile, to_tile
    raise "Start tile is not on the board"      if from_tile.is_a? NilTile
    raise "End tile is not on the board"        if to_tile.is_a? NilTile
    raise "Start tile does not contain a piece" if from_tile.empty?
    raise "This color is not at play"           if @game_state.color_is_at_play? from_tile.piece.color

    @move_engine.move_piece from_tile.piece, to_tile
    @game_state.switch_color_at_play
    print_board
  end

  def opponent_makes_move
    from_tile, to_tile = @opponent.decide_on_next_move

    make_move from_tile, to_tile
  end

  def print_board
    puts @board.to_s
  end

  def reset!
    @board = Board.new
    @move_engine = MoveEngine.new @board
    @game_state = GameState.new @board, @move_engine
  end

end