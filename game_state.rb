class GameState

  def initialize board, move_engine
    @board = board
    @move_engine = move_engine
    @color_at_play = :white
  end

  def switch_color_at_play
    @color_at_play = (@color_at_play == :white ? :black : :white)
  end

  def color_is_at_play? color
    @color_at_play == color
  end

end