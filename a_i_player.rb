class AIPlayer

  def initialize color, pieces, move_engine
    @color = color
    @pieces = pieces
    @move_engine = move_engine
  end

  def decide_on_next_move
    possible_moves = []

    while possible_moves.empty?
      piece = @pieces.choice
      possible_moves = @move_engine.calculate_possible_moves piece
    end

    return piece.tile, possible_moves.choice
  end


end