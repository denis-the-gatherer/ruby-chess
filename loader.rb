def reload
  load 'board.rb'
  load 'piece.rb'
  load 'tile.rb'
  load 'move_engine.rb'
  load 'test_run.rb'
  load 'game.rb'
  load 'game_state.rb'
  load 'a_i_player.rb'
end