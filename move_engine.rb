class MoveEngine

  def initialize board
    @board = board
  end

  def move_piece piece, tile_to_move_to
    raise "Piece cannot move here" unless is_legal_move_for_piece?(piece, tile_to_move_to)
    piece.tile.remove_piece
    tile_to_move_to.add_piece(piece)
  end

  def is_legal_move_for_piece? piece, tile_to_move_to
    calculate_possible_moves(piece).include?(tile_to_move_to) and future_board_state_does_not_lead_to_check?(piece, tile_to_move_to)
  end

  def calculate_possible_moves piece
    possible_moves = []
    x,y = piece.tile.coordinates

    case piece.type
    when :pawn
      move_ahead = (piece.orientation == :south) ? 1 : -1
      tile_ahead      = @board.tile_for_coordinates(x + move_ahead, y)
      two_tiles_ahead = @board.tile_for_coordinates(x + 2 * move_ahead, y)

      possible_moves << tile_ahead if tile_ahead.empty?
      possible_moves << two_tiles_ahead if two_tiles_ahead.empty? and (piece.tile.coordinates == piece.starting_coordinates)
      (tile_right_above = @board.tile_for_coordinates(x + move_ahead, y +1)).contains_piece_of_enemy_color?(piece.color) ? possible_moves << tile_right_above : nil
      (tile_left_above = @board.tile_for_coordinates(x + move_ahead, y -1)).contains_piece_of_enemy_color?(piece.color) ? possible_moves << tile_left_above : nil
    when :tower
      possible_moves = calculate_possible_moves_for_tower(piece)
    when :horse
      possible_coordinates_for_horse = [
        [x+2,y+1],
        [x+2,y-1],
        [x-2,y+1],
        [x-2,y-1],
        [x+1,y+2],
        [x-1,y+2],
        [x+1,y-2],
        [x-1,y-2]
      ]
      possible_coordinates_for_horse.each do |coordinates|
        tile_to_move_to = @board.tile_for_coordinates(coordinates[0],coordinates[1])
        possible_moves << tile_to_move_to if tile_to_move_to.piece_can_move_here? or tile_to_move_to.contains_piece_of_enemy_color?(piece.color)
      end
    when :rook
      possible_moves = calculate_possible_moves_for_rook(piece)
    when :queen
      possible_moves << calculate_possible_moves_for_rook(piece)
      possible_moves << calculate_possible_moves_for_tower(piece)
      possible_moves.flatten!
    when :king
      possible_coordinates_for_queen = calculate_surrounding_tile_coordinates(x,y)
      possible_coordinates_for_queen.each do |coordinates|
        tile_to_move_to = @board.tile_for_coordinates(coordinates[0],coordinates[1])
        possible_moves << tile_to_move_to if (tile_to_move_to.piece_can_move_here? or tile_to_move_to.contains_piece_of_enemy_color?(piece.color)) and tile_is_not_accessible_by_pieces_of_enemy_color?(tile_to_move_to, piece.color)
      end
    end

    possible_moves.compact
  end

  def tile_is_not_accessible_by_pieces_of_enemy_color? tile_to_move_to, color
    pieces_of_enemy_color = @board.all_pieces_of_enemy_color(color)
    return false if surrounding_tiles_contains_enemy_king?(tile_to_move_to, color)
    pieces_of_enemy_color.each do |enemy_piece|
      possible_moves = enemy_piece.type == :king ? [] : calculate_possible_moves(enemy_piece)
      possible_moves.each do |tile_piece_can_move_to|
        return false if tile_piece_can_move_to.coordinates == tile_to_move_to.coordinates
      end
    end
    return true
  end

  private

  def calculate_possible_moves_for_rook(piece)
    raise "This piece cannot make the moves of a rook!" unless piece.is_a_rook? or piece.is_a_queen?

    possible_moves = []
    x,y = piece.tile.coordinates
    i = 0
    cannot_move_north, cannot_move_south, cannot_move_east, cannot_move_west = false, false, false, false

    loop do
      i+=1
      unless cannot_move_north
        tile_to_move_to = @board.tile_for_coordinates(x + i, y + i)
        possible_moves << tile_to_move_to if tile_to_move_to.piece_can_move_here? or tile_to_move_to.contains_piece_of_enemy_color?(piece.color)
        cannot_move_north = !tile_to_move_to.piece_can_move_here?
      end
      unless cannot_move_south
        tile_to_move_to = @board.tile_for_coordinates(x - i, y + i)
        possible_moves << tile_to_move_to if tile_to_move_to.piece_can_move_here? or tile_to_move_to.contains_piece_of_enemy_color?(piece.color)
        cannot_move_south = !tile_to_move_to.piece_can_move_here?
      end
      unless cannot_move_east
        tile_to_move_to = @board.tile_for_coordinates(x + i, y - i)
        possible_moves << tile_to_move_to if tile_to_move_to.piece_can_move_here? or tile_to_move_to.contains_piece_of_enemy_color?(piece.color)
        cannot_move_east = !tile_to_move_to.piece_can_move_here?
      end
      unless cannot_move_west
        tile_to_move_to = @board.tile_for_coordinates(x - i, y - i)
        possible_moves << tile_to_move_to if tile_to_move_to.piece_can_move_here? or tile_to_move_to.contains_piece_of_enemy_color?(piece.color)
        cannot_move_west = !tile_to_move_to.piece_can_move_here?
      end
      break if cannot_move_east and cannot_move_west and cannot_move_south and cannot_move_north
    end

    possible_moves
  end

  def calculate_possible_moves_for_tower(piece)
    raise "This piece cannot make the moves of a tower!" unless piece.is_a_tower? or piece.is_a_queen?

    possible_moves = []
    x,y = piece.tile.coordinates
    i = 0
    cannot_move_north, cannot_move_south, cannot_move_east, cannot_move_west = false, false, false, false

    loop do
      i += 1
      unless cannot_move_north
        tile_to_move_to = @board.tile_for_coordinates(x + i, y)
        possible_moves << tile_to_move_to if tile_to_move_to.piece_can_move_here? or tile_to_move_to.contains_piece_of_enemy_color?(piece.color)
        cannot_move_north = !tile_to_move_to.piece_can_move_here?
      end
      unless cannot_move_south
        tile_to_move_to = @board.tile_for_coordinates(x - i, y)
        possible_moves << tile_to_move_to if tile_to_move_to.piece_can_move_here? or tile_to_move_to.contains_piece_of_enemy_color?(piece.color)
        cannot_move_south = !tile_to_move_to.piece_can_move_here?
      end
      unless cannot_move_east
        tile_to_move_to = @board.tile_for_coordinates(x, y + i)
        possible_moves << tile_to_move_to if tile_to_move_to.piece_can_move_here? or tile_to_move_to.contains_piece_of_enemy_color?(piece.color)
        cannot_move_east = !tile_to_move_to.piece_can_move_here?
      end
      unless cannot_move_west
        tile_to_move_to = @board.tile_for_coordinates(x, y - i)
        possible_moves << tile_to_move_to if tile_to_move_to.piece_can_move_here? or tile_to_move_to.contains_piece_of_enemy_color?(piece.color)
        cannot_move_west = !tile_to_move_to.piece_can_move_here?
      end
      break if cannot_move_east and cannot_move_west and cannot_move_south and cannot_move_north
    end

    possible_moves
  end

  def calculate_surrounding_tile_coordinates(x,y)
    [
      [x+1,y],
      [x-1,y],
      [x,y+1],
      [x,y-1],
      [x+1,y+1],
      [x+1,y-1],
      [x-1,y+1],
      [x-1,y-1]
    ]
  end

  def surrounding_tiles_contains_enemy_king?(tile, color)
    x,y = tile.coordinates
    calculate_surrounding_tile_coordinates(x,y).each do |coordinate|
      i,j = coordinate
      return true if @board.tile_for_coordinates(i,j).contains_enemy_king?(color)
    end
    return false
  end

  def future_board_state_does_not_lead_to_check?(piece, tile_to_move_to)
    cloned_board = @board.clone
    cloned_piece = cloned_board.get_tile_for_coordinates(piece.tile.coordinates[0], piece.tile.coordinate[1]).piece
    cloned_tile_to_move_to = cloned_board.get_tile_for_coordinates(tile_to_move_to.coordinates[0], tile_to_move_to.coordinates[1])
    move_engine = MoveEngine.new cloned_board
    move_engine.move_piece piece, tile_to_move_to
    move_engine.tile_is_not_accessible_by_pieces_of_enemy_color?(@board.king_of_color(piece.color).tile, piece.color)
  end

end