class Board

  attr_accessor :grid

  def initialize
    initialize_empty_board
    add_pieces_to_board
  end

  def initialize_empty_board
    self.grid = []
    8.times do |x|
      self.grid << []
      8.times do |y|
        color = (x+y).even? ? :black : :white
        self.grid[x] << Tile.new(x, y, color)
      end
    end
  end

  def add_pieces_to_board
    grid[0][0].add_piece(Piece.new(:tower, :white))
    grid[0][7].add_piece(Piece.new(:tower, :white))
    grid[0][1].add_piece(Piece.new(:horse, :white))
    grid[0][6].add_piece(Piece.new(:horse, :white))
    grid[0][2].add_piece(Piece.new(:rook, :white))
    grid[0][5].add_piece(Piece.new(:rook, :white))
    grid[0][3].add_piece(Piece.new(:queen, :white))
    grid[0][4].add_piece(Piece.new(:king, :white))
    8.times do |x|
      grid[1][x].add_piece(Piece.new(:pawn, :white, { :starting_coordinates => [1, x], :orientation => :south }))
    end

    grid[7][0].add_piece(Piece.new(:tower, :black))
    grid[7][7].add_piece(Piece.new(:tower, :black))
    grid[7][1].add_piece(Piece.new(:horse, :black))
    grid[7][6].add_piece(Piece.new(:horse, :black))
    grid[7][2].add_piece(Piece.new(:rook, :black))
    grid[7][5].add_piece(Piece.new(:rook, :black))
    grid[7][3].add_piece(Piece.new(:queen, :black))
    grid[7][4].add_piece(Piece.new(:king, :black))
    8.times do |x|
      grid[6][x].add_piece(Piece.new(:pawn, :black, { :starting_coordinates => [6, x], :orientation => :north }))
    end
  end

  def to_s
    string = ""

    grid.each do |row|
      32.times{ string <<  "-" }
      string << "\n"
      row.each do |tile|
        string << tile.to_s
      end
      string << "\n"
    end
    32.times{ string << "-" }

    string
  end

  def tiles
    grid.flatten
  end

  def pieces
    tiles.collect{|tile| tile.piece unless tile.piece.nil?}.compact
  end

  def pawns
    pieces.collect{|piece| piece if piece.type == :pawn}.compact
  end

  def towers
    pieces.collect{|piece| piece if piece.type == :tower}.compact
  end

  def horses
    pieces.collect{|piece| piece if piece.type == :horse}.compact
  end

  def rooks
    pieces.collect{|piece| piece if piece.type == :rook}.compact
  end

  def queens
    pieces.collect{|piece| piece if piece.type == :queen}.compact
  end

  def kings
    pieces.collect{|piece| piece if piece.type == :king}.compact
  end

  def king_of_color color
    pieces.collect{|piece| piece if piece.type == :king and piece.color == color}.compact.first
  end

  def all_pieces_of_color color
    pieces.collect{|piece| piece if piece.color == color}.compact
  end

  def all_pieces_of_enemy_color color
    pieces.collect{|piece| piece if piece.color != color}.compact
  end

  def tile_for_coordinates x,y
    return grid[x][y] unless grid[x].nil? or grid[x][y].nil? or x < 0 or y < 0
    return NilTile.new
  end

end