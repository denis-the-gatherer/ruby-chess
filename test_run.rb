class TestRun

  def self.move_pawns_at_random
    board = Board.new
    puts board.to_s
    TestRun.wait

    move_engine = MoveEngine.new board

    while true
      possible_moves = []
      while possible_moves.empty?
      puts "No move available"
        pawn = board.pawns.choice
        possible_moves = move_engine.calculate_possible_moves pawn
      end
      move_engine.move_piece pawn, possible_moves.choice
      puts board.to_s
      TestRun.wait
    end

    nil
  end

  def self.move_towers_at_random
    board = Board.new
    move_engine = MoveEngine.new board

    board.pawns.each do |p|
      p.tile.remove_piece
    end

    while true
      possible_moves = []
      while possible_moves.empty?
        puts "No move available"
        tower = board.towers.choice
        possible_moves = move_engine.calculate_possible_moves tower
      end
      move_engine.move_piece tower, possible_moves.choice
      puts board.to_s
      TestRun.wait
    end

    nil
  end

  def self.move_horses_at_random
    board = Board.new
    puts board.to_s
    TestRun.wait

    move_engine = MoveEngine.new board

    while true
      possible_moves = []
      while possible_moves.empty?
      puts "No move available"
        horse = board.horses.choice
        possible_moves = move_engine.calculate_possible_moves horse
      end
      move_engine.move_piece horse, possible_moves.choice
      puts board.to_s
      TestRun.wait
    end

    nil
  end

  def self.move_rooks_at_random
    board = Board.new
    move_engine = MoveEngine.new board

    board.pawns.each do |p|
      p.tile.remove_piece
    end

    while true
      possible_moves = []
      while possible_moves.empty?
        puts "No move available"
        rook = board.rooks.choice
        possible_moves = move_engine.calculate_possible_moves rook
      end
      move_engine.move_piece rook, possible_moves.choice
      puts board.to_s
      TestRun.wait
    end

    nil
  end

  def self.move_queens_at_random
    board = Board.new
    move_engine = MoveEngine.new board
    puts board.to_s
    TestRun.wait

    board.pawns.each do |p|
      p.tile.remove_piece
    end
    puts board.to_s
    TestRun.wait

    while true
      possible_moves = []
      while possible_moves.empty?
        puts "No move available"
        queen = board.queens.choice
        possible_moves = move_engine.calculate_possible_moves queen
      end
      move_engine.move_piece queen, possible_moves.choice
      puts board.to_s
      TestRun.wait
    end

    nil
  end

  def self.move_kings_at_random
    board = Board.new
    move_engine = MoveEngine.new board
    puts board.to_s
    TestRun.wait

    board.pawns.each do |p|
      p.tile.remove_piece
    end
    puts board.to_s
    TestRun.wait

    while true
      possible_moves = []
      while possible_moves.empty?
        puts "No move available"
        king = board.kings.choice
        possible_moves = move_engine.calculate_possible_moves king
      end
      move_engine.move_piece king, possible_moves.choice
      puts board.to_s
      TestRun.wait
    end

    nil
  end

  def self.wait
    sleep 1
  end

  def self.get_ai_player
    @board.all_pieces_of_color color
  end

end