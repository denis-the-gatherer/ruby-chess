class Piece

attr_accessor :type, :tile, :orientation, :color, :starting_coordinates

  PIECES = {
    :pawn =>{
      :name => 'pawn',
      :character => 'P'
    },
    :tower =>{
      :name => 'tower',
      :character => 'T'
    },
    :horse =>{
      :name => 'horse',
      :character => 'H'
    },
    :rook =>{
      :name => 'rook',
      :character => 'R'
    },
    :queen =>{
      :name => 'queen',
      :character => 'Q'
    },
    :king =>{
      :name => 'king',
      :character => 'K'
    }
  }

  COLORS = {
    :white => {
      :character => 'W'
    },
    :black => {
      :character => 'B'
    }
  }

  def initialize piece, color, options = {}
    raise "Piece does not exists" if PIECES[piece].nil?
    raise "Color does not exists" if COLORS[color].nil?
    self.type         = piece
    self.color        = color
    self.orientation  = options[:orientation]
    self.starting_coordinates = options[:starting_coordinates]
  end

  def to_s
    "#{COLORS[self.color][:character]}#{PIECES[self.type][:character]}"
  end

  def is_a_rook?
    return true if self.type == :rook
    return false
  end

  def is_a_queen?
    return true if self.type == :queen
    return false
  end

  def is_a_tower?
    return true if self.type == :tower
    return false
  end


end